#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  main.py
#  
#  Copyright 2012  <furas@tlen.pl>
#

import os
import sys
import wx

# dodanie podakatalogu z plikami tak aby dzialalo takze przy symbolic link
#global program_dir
program_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
sys.path.append(os.path.join(program_dir, "libs"))

import MainFrame

#----------------------------------------------------------------------

def main():
	app = wx.App()
	MainFrame.MainFrame(None, program_dir)	
	app.MainLoop()

#----------------------------------------------------------------------

if __name__ == '__main__':
	main()
