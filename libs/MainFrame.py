#!/usr/bin/python
# -*- coding: utf-8 -*-

import wx
import database
import os
import codecs
import csv

import task, container

import MyTreeCtrl, MyTreeCtrl2

class MainFrame(wx.Frame):
	
	treeCtrl = []
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def __init__(self, parent, program_dir):
		#super(MainFrame, self).__init__(parent, "wxToDo")
		
		'''
		
		w = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
		h = wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)

		pos=(w/2, h/2)
		print pos
		'''
		
		wx.Frame.__init__(self, None, title='wxToDo', pos=(600,600), size=(800,600))
		
		#self.Centre()
		self.program_dir = program_dir
		
		self.InitUI()

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def InitUI(self):
		self.CreateUIMenu()
		self.CreateUIStatusBar()
		self.CreateUITabs()
		
		self.Show(True)
		self.statusBar.SetStatusText('testujemy')

	#------------------------------------------------------------------
	# tworzenie menu
	#------------------------------------------------------------------
	
	def CreateUIMenu(self):
		
		# pasek menu
		self.menuBar =  wx.MenuBar()

		# menu "Pliki"
		self.menuFile = wx.Menu()
		self.menuFileNew  = self.menuFile.Append(wx.ID_NEW,  'Nowy plik', 'Stworzenie nowego dokumentu')
		self.menuFileOpen = self.menuFile.Append(wx.ID_OPEN, 'Otwórz plik', 'Otworzenie dokumentu')
		
		self.ID_LOADCSV = wx.NewId()
		self.menuFileLoadCSV = self.menuFile.Append(self.ID_LOADCSV, 'Wczytaj plik CSV', 'Otworzenie dokumentu CSV')
		
		self.menuFileSave = self.menuFile.Append(wx.ID_SAVE, 'Zapisz plik', 'Zapisanie dokumentu')

		self.ID_EXPORT = wx.NewId()
		self.menuFileExport = self.menuFile.Append(self.ID_EXPORT, 'Export do CSV', 'Exportowanie do CSV')
		
		self.menuFileQuit = self.menuFile.Append(wx.ID_EXIT, 'Zakończ', 'Wyjście z aplikacji')
		self.menuBar.Append(self.menuFile, '&Plik')
		
		# menu "Edycja"
		self.menuEdit = wx.Menu()
		self.menuBar.Append(self.menuEdit, '&Edycja')
		
		# menu "Pomoc"
		self.menuHelp = wx.Menu()
		self.menuBar.Append(self.menuHelp, 'Pomo&c')
		
		# przypisanie funkcji do elementow menu
		self.Bind(wx.EVT_MENU, self.OnFileNew, self.menuFileNew)
		self.Bind(wx.EVT_MENU, self.OnLoadCSV, self.menuFileLoadCSV)
		self.Bind(wx.EVT_MENU, self.OnSave, self.menuFileSave)
		self.Bind(wx.EVT_MENU, self.OnExport, self.menuFileExport)
		self.Bind(wx.EVT_MENU, self.OnQuit, self.menuFileQuit)
		
		# aktywowanie menu
		self.SetMenuBar(self.menuBar)
	
	#------------------------------------------------------------------
	# tworzenie statusu
	#------------------------------------------------------------------
	
	def CreateUIStatusBar(self):
		self.statusBar = self.CreateStatusBar()

	#------------------------------------------------------------------
	# tworzenie panelu na zakladki 
	#------------------------------------------------------------------
	
	def CreateUITabs(self):
		self.notebook = wx.Notebook(self)
		self.AddTab("AutoOpen")
		self.AddEmptyTab("Puste")

	#------------------------------------------------------------------
	# dodanie zakladki z danymi z podanego plik
	#------------------------------------------------------------------

	def AddTab(self, nazwa):
		
		tempTreeCtrl = MyTreeCtrl.MyTreeCtrl(self.notebook, os.path.join(self.program_dir,'db/hublist.sqlite')) 
		tempTreeCtrl.name = nazwa

		self.treeCtrl.append(tempTreeCtrl)

		self.notebook.AddPage(tempTreeCtrl, nazwa)

	#------------------------------------------------------------------
	# tworzenie pustej zakladki
	#------------------------------------------------------------------

	def AddEmptyTab(self, nazwa):
		
		tempTreeCtrl = MyTreeCtrl2.MyTreeCtrl2(self.notebook) 
		tempTreeCtrl.name = nazwa
		
		self.treeCtrl.append(tempTreeCtrl)
	
		self.notebook.AddPage(tempTreeCtrl, nazwa)

	#------------------------------------------------------------------
	# zamykanie programu
	#------------------------------------------------------------------
	
	def OnQuit(self, e):

		# zamykanie wszystkich zakladek po kolei
		for el in self.treeCtrl:
			el.Close()
			
		# zamkniecie programu
		self.Close()

	#------------------------------------------------------------------
	# zapisywanie (pierwszej) zakladki do CSV
	#------------------------------------------------------------------

	def OnSave(self, e):
		
		wx.MessageBox("Zapis do CSV")
		#for el in self.treeCtrl:
		#	el.Save("nic ważnego")
		if self.treeCtrl[0].Save('nic') == False:
			wx.MessageBox("Problem z zapisem")

	#------------------------------------------------------------------
	# zapisywanie (pierwszej) zakladki do CSV
	#------------------------------------------------------------------

	def OnExport(self, e):
		
		nb = self.notebook
		sel = nb.GetSelection()
		wx.MessageBox("Export do CSV " + nb.GetPageText(sel) + " " + self.treeCtrl[sel].fullname)
		#for el in self.treeCtrl:
		#	el.Save("nic ważnego")
		#self.treeCtrl[0].Save('nic')

	#------------------------------------------------------------------
	# dodanie nowej zakladki 
	#------------------------------------------------------------------
	
	def OnFileNew(self, e):
		dlg = wx.TextEntryDialog(self, "Nazwa:","Tworzenie")
		if dlg.ShowModal() == wx.ID_OK :
			nazwa = dlg.GetValue()
			if nazwa == '' :
				wx.MessageBox("Niepoprawna nazwa","Tworzenie")
			else:
				self.AddEmptyTab(nazwa)


	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def OnLoadCSV(self, e):
		
		dlg = wx.FileDialog(self, "Wczytanie CSV", wildcard = "CSV (*.csv)|*.csv")
		
		if dlg.ShowModal() == wx.ID_OK:
			path = dlg.GetPath()
			self.LoadCSV(path)
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def LoadCSV(self, filename):
		
		# otwarcie pliku
		
		ifile = codecs.open(filename, 'r+', encoding="utf-8")
		reader = csv.reader(ifile)
		
		# pomijanie naglowka 
		reader.next()
		
		# wczytanie wierszy
		
		rows = {}
		
		for row in reader:
			rows[row[0]] = row
		
		tempTreeCtrl = MyTreeCtrl2.MyTreeCtrl2(self.notebook, filename, rows) 
		tempTreeCtrl.name = filename

		self.treeCtrl.append(tempTreeCtrl)

		self.notebook.AddPage(tempTreeCtrl, filename)
				
		# zamkniecie pliku
		
		ifile.close()
				
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
