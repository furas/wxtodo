#!/usr/bin/python
# -*- coding: utf-8 -*-

import wx
import database
import task, container
import csv
import uuid
import os
import codecs
import json

class MyTreeCtrl2(wx.TreeCtrl):
	
	fullname = None
	
	items = []
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def __init__(self, parent, fullname = None, data = None):
		super(MyTreeCtrl2, self).__init__(parent, 1, wx.DefaultPosition, (-1,-1),
			#wx.TR_HIDE_ROOT |
			wx.TR_HAS_VARIABLE_ROW_HEIGHT | 
			wx.TR_HAS_BUTTONS | 
			#wx.TR_FULL_ROW_HIGHLIGHT | 
			wx.TR_MULTIPLE | 
			wx.TR_EDIT_LABELS |
			wx.TR_DEFAULT_STYLE 
		)
		
		self.InitUI()
		
		self.fullname = fullname
		
		if fullname != None:
			#self.LoadData(fullname)
			pass
		else:
			self.AddMyRoot()

		if data != None:
			pass
			data = sorted(data)
			print '\n'.join(data)
			
			
			
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def InitUI(self):
		
		self.imageList = wx.ImageList(16, 16)
		
		self.imageId = {}
		
		#self.imageId['root']  = self.imageList.Add(wx.Image('tango/16x16/apps/system-file-manager.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		#self.imageId['root']  = self.imageList.Add( wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK, wx.ART_TOOLBAR, (16,16)) )
		self.imageId['root']  = self.imageList.Add( wx.ArtProvider.GetBitmap('system-file-manager', wx.ART_OTHER, (16,16)) )

		#self.imageId['folder']  = self.imageList.Add(wx.Image('tango/16x16/places/folder.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		self.imageId['folder']  = self.imageList.Add( wx.ArtProvider.GetBitmap('folder', wx.ART_OTHER, (16,16)) )
		
		#self.imageId['lista']   = self.imageList.Add(wx.Image('tango/16x16/actions/edit-paste.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		self.imageId['lista']  = self.imageList.Add( wx.ArtProvider.GetBitmap('edit-paste', wx.ART_OTHER, (16,16)) )
		
		#self.imageId['task'] = self.imageList.Add(wx.Image('tango/16x16/actions/appointment-new.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		self.imageId['task']  = self.imageList.Add( wx.ArtProvider.GetBitmap('appointment-new', wx.ART_OTHER, (16,16)) )
		
		self.AssignImageList(self.imageList)
		
		#self.Bind(wx.EVT_LEFT_DCLICK, self.OnDoubleClick)
		#self.Bind(wx.EVT_LEFT_UP, self.OnClick)
		self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.OnClick)
		self.Bind(wx.EVT_CONTEXT_MENU, self.OnRightClick)
		
		self.popupmenu = wx.Menu()		
		self.Bind(wx.EVT_MENU, self.OnAddFolder, self.popupmenu.Append(-1, 'Dodaj Folder'))
		self.Bind(wx.EVT_MENU, self.OnAddLista, self.popupmenu.Append(-1, 'Dodaj Listę'))
		self.Bind(wx.EVT_MENU, self.OnAddTask, self.popupmenu.Append(-1, 'Dodaj Zadanie'))
		self.Bind(wx.EVT_MENU, self.OnDelete, self.popupmenu.Append(-1, 'Usuń'))
		self.popupmenu.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.OnEdit, self.popupmenu.Append(-1, 'Edytuj'))
		self.Bind(wx.EVT_MENU, self.OnCopyText, self.popupmenu.Append(-1, 'Skopiuj tekst'))
		self.Bind(wx.EVT_MENU, self.OnPasteText, self.popupmenu.Append(-1, 'Wklej tekst'))
		self.popupmenu.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.OnSaveSelected, self.popupmenu.Append(-1, 'Zapisz gałąź'))
		self.Bind(wx.EVT_MENU, self.OnSaveRoot, self.popupmenu.Append(-1, 'Zapisz całość'))
		self.popupmenu.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.OnShowInfo, self.popupmenu.Append(-1, 'Właściwości'))
		
	#------------------------------------------------------------------
	# rozwijanie/zwijanie galezi drzewa
	#------------------------------------------------------------------
	
	def OnDoubleClick(self, e):
		
		selected = e.GetEventObject().GetSelection()

		if self.IsExpanded(selected):
			self.Collapse(selected)
		else:
			self.Expand(selected)
		
	#------------------------------------------------------------------

	def OnClick(self, e):
		print '=== OnClick ==='
		obj = e.GetEventObject()
		
		selected = obj.GetSelection()
		
		print 'obj name:', obj.GetName()
		
		if selected == self.root:
			print 'ROOT !!!'
			
		print 'text:', self.GetItemText(selected)
		dane = self.GetPyData(selected)
		if dane[2] == None:
			print 'dane:', 'None'
		else:
			print 'dane:'
			for key in dane[2].values:
				print '    ', key + ':', dane[2].values[key]
		
		
	#------------------------------------------------------------------
	# menu kontekstowe dla wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnRightClick(self, e):
		self.PopupMenu(self.popupmenu)
		
	#------------------------------------------------------------------
	# dodanie nowego folderu do wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnAddFolder(self, event):
		text = wx.GetTextFromUser('Nazwa folderu:', 'Dodawanie folderu', 'Nowy Folder');
		if text != '':
			self.AddFolder(uuid.uuid4(), self.GetSelection(), text)
		
	#------------------------------------------------------------------
	# dodanie nowej listy do wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnAddLista(self, event):
		text = wx.GetTextFromUser('Nazwa listy:', 'Dodawanie listy', 'Nowa Lista');
		if text != '':
			self.AddLista(uuid.uuid4(), self.GetSelection(), text)
		
	#------------------------------------------------------------------
	# dodanie nowego zadania do wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnAddTask(self, event):
		text = wx.GetTextFromUser('Nazwa zadania:', 'Dodawanie zadania', 'Nowe Zadanie');
		if text != '':
			self.AddTask(uuid.uuid4(), self.GetSelection(), text)
		
	#------------------------------------------------------------------
	# kopiowanie tekstu do schowka
	#------------------------------------------------------------------

	def OnCopyText(self, event):
		
		clip = wx.Clipboard().Get()
		
		if clip.Open():
		
			text = self.GetItemText(self.GetSelection())
			
			dataObject = wx.TextDataObject()
			dataObject.SetText(text)
			
			clip.SetData(dataObject) # TODO: test wstawienia
			
			clip.Flush()
			clip.Close()
			
		else:
			wx.MessageBox('Problem z dostępem do schowka', 'Kopiowanie Tekstu')
		
	#------------------------------------------------------------------
	# wstawianie tekstu ze schowka
	#------------------------------------------------------------------

	def OnPasteText(self, event):
		
		clip = wx.Clipboard().Get()
		
		if clip.Open():
		
			dataObject = wx.TextDataObject()
			
			clip.GetData(dataObject) # TODO: test pobrania
			
			text = dataObject.GetText()
			
			self.SetItemText(self.GetSelection(), text)
			
			clip.Flush()
			clip.Close()
			
		else:
			wx.MessageBox('Problem z dostępem do schowka', 'Kopiowanie Tekstu')

	#------------------------------------------------------------------
	# edycja wybranego elementu
	#------------------------------------------------------------------

	def OnEdit(self, event):
		
		selected = self.GetSelection()
		
		dlg = wx.TextEntryDialog(self, 'Tekst:', 'Edycja', self.GetItemText(selected) )
		
		if dlg.ShowModal() == wx.ID_OK :
			self.SetItemText(selected, dlg.GetValue())

	#------------------------------------------------------------------
	# kasowanie wybranego elementu i jego podelementow
	#------------------------------------------------------------------
		
	def OnDelete(self, event):
		
		dlg = wx.MessageDialog(self, 'Skasować ?', 'Usuwanie')
		
		if dlg.ShowModal() == wx.ID_OK:
			self.DeleteElement(self.GetSelection())
		
	#------------------------------------------------------------------
		
	def OnShowInfo(self, event):
		# wybrany wezel
		selected = self.GetSelection()
		
		# dane w wezle
		text = self.GetItemText(selected) 
		pyData = self.GetPyData(selected)
		pyData_keys = sorted(pyData)
		
		pyDataText = '\n'.join(['%s: %s' % (key, pyData[key]) for key in pyData_keys])
		
		# pokazanie danych
		wx.MessageBox('text: %s\n--- dane ---\n%s' % (text, pyDataText), "Właściwości")
		
	#------------------------------------------------------------------
	# 
	#------------------------------------------------------------------
	
	def OnSaveSelected(self, event):
		selected = self.GetSelection()
		self.SaveSelected('selected', selected)
		print '== Save =='
	
	#------------------------------------------------------------------
	# 
	#------------------------------------------------------------------
	
	def OnSaveRoot(self, event):
		selected = self.GetRootItem()
		#self.SaveSelected('selected', selected)
		print '== Save - start =='
		self.Save('selected', selected)
		print '== Save - stop =='
	
	###################################################################
	#
	###################################################################
	
	#------------------------------------------------------------------
	# 
	#------------------------------------------------------------------
		
	def ReadRoot(self):

		# pobranie korzenia drzewa
		rows = self.db.GetTreeRoot()
		
		# TODO: rozpatrywanie przypadku wielu korzeni
		if not rows:
			#print 'Brak elemtnu'
			pass
		else:
			# stworzenie kontenera na korzen
			kontener = container.Container(rows[0])

			# dodanie korzenia do drzewa
			self.root = self.AddRoot(kontener.values['name'])
			self.SetPyData(self.root,('root', '-', None))
			self.SetItemImage(self.root, self.imageId['root'], wx.TreeItemIcon_Normal)
			
			#print 'parent:', kontener.values['parent']
			#print 'children:'
			
			# przetwarzanie dzieci
			for child in kontener.values['children']:
				#print child['type'], child['id']
				if child['type'] == 'folder':
					self.ReadFolder( child['id'], self.root)
		
	#------------------------------------------------------------------
	
	def AddMyRoot(self, nazwa = 'root'):
		# stworzenie kontenera na korzen
		#kontener = container.Container()

		# dodanie korzenia do drzewa
		self.root = self.AddRoot(nazwa)
		
		# wypelnienie elementu danymi
		self.SetPyData(self.root, {'type':'root', 'id':0, 'parent_id':None})
		
		# dodanie ikonki do elementu		
		self.SetItemImage(self.root, self.imageId['root'], wx.TreeItemIcon_Normal)
		
	#------------------------------------------------------------------
	# dodawanie roznych elementow do drzewa
	#------------------------------------------------------------------

	def AddFolder(self, item_id, parent, name = 'Nowy Folder', data = None):
		
		# id rodzica
		parent_pyData = self.GetPyData(parent)
		parent_id = parent_pyData['id']
		
		# tworzenie nowego elementu w drzewie
		element = self.AppendItem(parent, name)
		self.Expand(parent)
		
		# wypelnienie elementu danymi
		self.SetPyData(element, {'type':'folder', 'id':str(item_id), 'parent_id':str(parent_id), 'data':data})
		
		# dodanie ikonki do elementu
		self.SetItemImage(element, self.imageId['folder'], wx.TreeItemIcon_Normal)
		
		return element
			
	#------------------------------------------------------------------
		
	def AddLista(self, item_id, parent, name = 'Nowa Lista', data = None):
		
		# id rodzica
		parent_pyData = self.GetPyData(parent)
		parent_id = parent_pyData['id']
		
		# tworzenie nowego elementu w drzewie
		element = self.AppendItem(parent, name)
		self.Expand(parent)
		
		# wypelnienie elementu danymi
		self.SetPyData(element, {'type':'list', 'id':str(item_id), 'parent_id':str(parent_id), 'data':data})
		
		# dodanie ikonki do elementu
		self.SetItemImage(element, self.imageId['lista'], wx.TreeItemIcon_Normal)
		
		return element
			
	#------------------------------------------------------------------
		
	def AddTask(self, item_id, parent, name = 'Nowe Zadanie', data = None):
		
		# id rodzica
		parent_pyData = self.GetPyData(parent)
		parent_id = parent_pyData['id']
		
		# tworzenie nowego elementu w drzewie
		element = self.AppendItem(parent, name)
		self.Expand(parent)
		
		# wypelnienie elementu danymi
		self.SetPyData(element, {'type':'task', 'id':str(item_id), 'parent_id':str(parent_id), 'data':data})
		
		# dodanie ikonki do elementu
		self.SetItemImage(element, self.imageId['task'], wx.TreeItemIcon_Normal)
		
		return element
			
	#------------------------------------------------------------------
	# usuwanie roznych elementow z drzewa
	#------------------------------------------------------------------

	def DeleteElement(self, item):
		
		# sprawdzenie czy element ma dzieci
		if self.ItemHasChildren(item):
			wx.MessageBox('element ma podelementy !!!')
			# TODO: pytanie co zrobić
		else:
			self.Delete(item)
			
	#------------------------------------------------------------------
	# wypisywanie (tekstow) elementow drzewa na ekranie i do pliku
	#------------------------------------------------------------------

	def PrintTreeItemText(self, selected, poziom):
		
		# tekst do zapisu
		text = self.GetItemText(selected)

		# wypisanie na ekran
		print text
		
		# tworzenie tuple z pustymi miejscami aby otrzymac wciecia
		for i in range(poziom):
			self.ofile.write(',')
			
		# zapisanie do pliku
		self.ofile.write('"%s"\n' % text)
		
		# sprawdzanie czy jest potomek
		child, cookie = self.GetFirstChild(selected)
		
		while child.IsOk():
			# przetwarzanie potomków
			self.PrintTreeItemText(child, poziom+1)
			
			# kolejny potomek 
			child, cookie = self.GetNextChild(selected, cookie)
	
	#------------------------------------------------------------------
	# zapis calego drzewa do CSV
	#------------------------------------------------------------------

	def Save(self, filename = None, selected = None):
		
		# agumenty - nazwa pliku
		
		if filename == None:
			filename = self.fullname
			
		if filename == None:
			return False
			
		# agumenty - zapisywana galaz
		
		if selected == None:
			selected = self.root
			
		if selected == None:
			return False
			
		# otwarcie pliku
		
		self.ofile = codecs.open( filename + '.csv','w+', encoding="utf-8")
		self.writer = csv.writer(self.ofile)

		# zapis naglowka 
		self.writer.writerow( ['id', 'parent_id', 'type', 'text', 'data'] )
		
		# zapis elementow
		
		self.SaveTreeItem(selected, None, 0)
		
		# zamkniecie pliku
		
		self.writer.close()
		self.ofile.close()
		
		# koniec
		
		return True
	
	#------------------------------------------------------------------
	# zapis calego drzewa do CSV
	#------------------------------------------------------------------

	def Load(self, filename = None, selected = None):
		
		# agumenty - nazwa pliku
		
		if filename == None:
			filename = self.fullname
			
		if filename == None:
			return False
			
		# agumenty - zapisywana galaz
		
		if selected == None:
			selected = self.root
			
		if selected == None:
			return False
			
		# otwarcie pliku
		
		self.ifile = codecs.open( filename + '.csv','w+', encoding="utf-8")
		self.reader = csv.reader(self.ifile)

		# pomijanie naglowka 
		self.reader.next()
		
		# zapis elementow
		
		self.LoadTreeItem(selected, None, 0)
		
		# zamkniecie pliku
		
		self.reader.close()
		self.ofile.close()
		
		# koniec
		
		return True
	
	#------------------------------------------------------------------
	
	def SaveTreeItem(self, selected, parent_id, item_id):
		# tekst do zapisu
		text = self.GetItemText(selected)
		
		# dane do zapisu
		pyData = self.GetPyData(selected)
		
		# id rodzica
		parent = self.GetItemParent(selected)
		parent_pyData = self.GetPyData(parent)
		parent_id = None if parent_pyData == None else parent_pyData['id'] 
		
		print 'parent_id: %s / %s' % (parent_id, parent_pyData)
		
		# wypisanie na ekran
		print '"%s", "%s", "%s", "%s", "%s"' % (str(item_id), str(parent_id), pyData['type'], text, json.dumps(pyData) )
		
		# zapisanie do pliku
		self.writer.writerow( [item_id, parent_id, pyData['type'], text, json.dumps(pyData)] )
		
		# sprawdzanie czy jest potomek
		child, cookie = self.GetFirstChild(selected)
		
		last_item_id = item_id
		
		while child.IsOk():
			
			# przetwarzanie potomków
			last_item_id = self.SaveTreeItem(child, item_id, last_item_id+1)
			
			# kolejny potomek 
			child, cookie = self.GetNextChild(selected, cookie)
		
		return last_item_id
		
	#------------------------------------------------------------------
	# zapis wybranej galezi do CSV
	#------------------------------------------------------------------

	def SaveSelected(self, filename, selected):
		
		self.Save(selected = selected)
		
		return True
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
