# -*- coding: utf-8 -*-

import sqlite3

class Database(object):

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def __init__(self, db_file = 'db/hublist.sqlite'):
		
		self.conn = sqlite3.connect(db_file)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def Close(self):
		self.conn.close()
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def Execute(self, query, arguments = None):
		
		c = self.conn.cursor()
		if arguments:
			c.execute(query, arguments)
		else:
			c.execute(query)
		
		return c.fetchall()

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetTasks(self, limit = None):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'task%\''

		if( limit != None ):
			query += ' LIMIT ' + limit
		
		return self.Execute(query)

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetContainers(self, limit = None):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'containers%\''

		if( limit != None ):
			query += ' LIMIT ' + limit
		
		return self.Execute(query)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetViewstates(self, limit = None):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'viewstates%\''

		if( limit != None ):
			query += ' LIMIT ' + limit
		
		return self.Execute(query)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetStats(self, limit = None):
		query = 'SELECT * FROM itemtable WHERE key LIKE \'stats%\''

		if( limit != None ):
			query += ' LIMIT ' + limit
		
		return self.Execute(query)

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetOthers(self, limit = None):
		
		query = 'SELECT * \
			 FROM itemtable \
			 WHERE key NOT LIKE \'task%\' \
			 AND key NOT LIKE \'containers%\' \
			 AND key NOT LIKE \'viewstates%\' \
			 AND key NOT LIKE \'stats%\''

		if( limit != None ):
			query += ' LIMIT ' + limit
		
		return self.Execute(query)
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def GetTreeRoot(self):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'containers%\' AND value LIKE \'%"parent":null%\''

		return self.Execute(query)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetTreeElement(self, parent):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'containers%\' AND value LIKE \'%"parent":' + parent + '%\''
		#print 'query:', query
		
		return self.Execute(query)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def GetTreeFolder(self, numer):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'containers::' + numer +'\''
		#print 'query:', query
		
		return self.Execute(query)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def GetTreeList(self, numer):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'containers::' + numer +'\''
		#print 'query:', query
		
		return self.Execute(query)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetTreeTask(self, numer):
		
		query = 'SELECT * FROM itemtable WHERE key LIKE \'tasks::' + numer +'\';'
		#print 'query:', query
		
		return self.Execute(query)
		
		#result = self.Execute(query)
		
		#print result
		
		#return result
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def SetTreeTask(self, numer, dane):
			
		query = 'INSERT OR REPLACE INTO itemtable VALUES(?, ?);' 

		result = self.Execute(query, (numer, dane))
	
		self.conn.commit()
	
		#print result
	
		return result
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def CreateTable(self):
		query = 'DROP TABLE IF EXISTS ItemTable;'
		result = self.Execute(query)
		query = 'CREATE TABLE ItemTable (key TEXT UNIQUE ON CONFLICT REPLACE, value TEXT NOT NULL ON CONFLICT FAIL);'
		result = self.Execute(query)
		
