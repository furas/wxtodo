#!/usr/bin/python
# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-
# encoding: utf-8
# coding: utf-8

import wx
import database
import task, container
import csv
import uuid
import os
import codecs

class MyTreeCtrl(wx.TreeCtrl):
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def __init__(self, parent, home_dir):
		super(MyTreeCtrl, self).__init__(parent, 1, wx.DefaultPosition, (-1,-1),
			#wx.TR_HIDE_ROOT |
			wx.TR_HAS_VARIABLE_ROW_HEIGHT | 
			wx.TR_HAS_BUTTONS | 
			#wx.TR_FULL_ROW_HIGHLIGHT | 
			wx.TR_MULTIPLE | 
			wx.TR_EDIT_LABELS |
			wx.TR_DEFAULT_STYLE 
		)
		
		self.home_dir = home_dir
		
		self.InitUI()
		
		self.db = database.Database(os.path.join(self.home_dir,'db/hublist.sqlite'))
		
		self.db_write = database.Database(os.path.join(self.home_dir,'db/new.todo.sqlite'))
		self.db_write.CreateTable()
		
		self.LoadData()

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def InitUI(self):
		
		self.imageList = wx.ImageList(16, 16)
		
		self.imageId = {}
		
		#self.imageId['root']  = self.imageList.Add(wx.Image('tango/16x16/apps/system-file-manager.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		#self.imageId['root']  = self.imageList.Add( wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK, wx.ART_TOOLBAR, (16,16)) )
		self.imageId['root']  = self.imageList.Add( wx.ArtProvider.GetBitmap('system-file-manager', wx.ART_OTHER, (16,16)) )

		#self.imageId['folder']  = self.imageList.Add(wx.Image('tango/16x16/places/folder.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		self.imageId['folder']  = self.imageList.Add( wx.ArtProvider.GetBitmap('folder', wx.ART_OTHER, (16,16)) )
		
		#self.imageId['lista']   = self.imageList.Add(wx.Image('tango/16x16/actions/edit-paste.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		self.imageId['lista']  = self.imageList.Add( wx.ArtProvider.GetBitmap('edit-paste', wx.ART_OTHER, (16,16)) )
		
		#self.imageId['task'] = self.imageList.Add(wx.Image('tango/16x16/actions/appointment-new.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap() )
		self.imageId['task']  = self.imageList.Add( wx.ArtProvider.GetBitmap('appointment-new', wx.ART_OTHER, (16,16)) )
		
		self.AssignImageList(self.imageList)
		
		#self.Bind(wx.EVT_LEFT_DCLICK, self.OnDoubleClick)
		#self.Bind(wx.EVT_LEFT_UP, self.OnClick)
		self.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.OnClick)
		self.Bind(wx.EVT_CONTEXT_MENU, self.OnRightClick)
		
		
		self.popupmenu = wx.Menu()		
		self.Bind(wx.EVT_MENU, self.OnAddFolder, self.popupmenu.Append(-1, 'Dodaj Folder'))
		self.Bind(wx.EVT_MENU, self.OnAddLista, self.popupmenu.Append(-1, 'Dodaj Listę'))
		self.Bind(wx.EVT_MENU, self.OnAddTask, self.popupmenu.Append(-1, 'Dodaj Zadanie'))
		self.popupmenu.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.OnSaveSelected, self.popupmenu.Append(-1, 'Zapisz gałąź'))
		self.popupmenu.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.OnCopyText, self.popupmenu.Append(-1, 'Skopiuj tekst'))
		self.Bind(wx.EVT_MENU, self.OnPasteText, self.popupmenu.Append(-1, 'Wklej tekst'))
		self.popupmenu.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.OnEdit, self.popupmenu.Append(-1, 'Edytuj'))
		self.Bind(wx.EVT_MENU, self.OnDelete, self.popupmenu.Append(-1, 'Usuń'))
		self.popupmenu.AppendSeparator()
		self.Bind(wx.EVT_MENU, self.OnShowInfo, self.popupmenu.Append(-1, 'Właściwości'))
		
	#------------------------------------------------------------------
	# rozwijanie/zwijanie galezi drzewa
	#------------------------------------------------------------------
	
	def OnDoubleClick(self, e):
		
		selected = e.GetEventObject().GetSelection()

		if self.IsExpanded(selected):
			self.Collapse(selected)
		else:
			self.Expand(selected)
		
	#------------------------------------------------------------------

	def OnClick(self, e):
		print '=== OnClick ==='
		obj = e.GetEventObject()
		
		selected = obj.GetSelection()
		
		print 'obj name:', obj.GetName()
		
		if selected == self.root:
			print 'ROOT !!!'
			
		print 'text:', self.GetItemText(selected)
		dane = self.GetPyData(selected)
		if dane[2] == None:
			print 'dane:', 'None'
		else:
			print 'dane:'
			for key in dane[2].values:
				print '    ', key + ':', dane[2].values[key]
		
		
	#------------------------------------------------------------------
	# menu kontekstowe dla wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnRightClick(self, e):
		self.PopupMenu(self.popupmenu)
		
	#------------------------------------------------------------------
	# dodanie nowego folderu do wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnAddFolder(self, event):
		text = wx.GetTextFromUser('Nazwa folderu:', 'Dodawanie folderu', 'Nowy Folder');
		if text != '':
			self.AddFolder(uuid.uuid4(), self.GetSelection(), text)
		
	#------------------------------------------------------------------
	# dodanie nowej listy do wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnAddLista(self, event):
		text = wx.GetTextFromUser('Nazwa listy:', 'Dodawanie listy', 'Nowa Lista');
		if text != '':
			self.AddLista(uuid.uuid4(), self.GetSelection(), text)
		
	#------------------------------------------------------------------
	# dodanie nowego zadania do wybranego elementu drzewa
	#------------------------------------------------------------------

	def OnAddTask(self, event):
		nazwa = wx.GetTextFromUser('Nazwe zadanie:', 'Dodawanie zadania', 'Nowe Zadanie');
		if nazwa != '':
			numer = str(uuid.uuid4())
			parent = self.GetSelection()
			dane = self.GetPyData(parent)
			print dane
			print dane[2].toString()
			dane[2].values['children'].append({u'type':u'task',u'id':numer})
			self.SetPyData(parent, dane)
			zadanie = task.Task()
			zadanie.SetDatas('tasks::' + numer, nazwa, dane[1])
			self.AddTask(numer, parent, nazwa, zadanie)
		
	#------------------------------------------------------------------
	# kopiowanie tekstu do schowka
	#------------------------------------------------------------------

	def OnCopyText(self, event):
		
		clip = wx.Clipboard().Get()
		
		if clip.Open():
		
			text = self.GetItemText(self.GetSelection())
			
			dataObject = wx.TextDataObject()
			dataObject.SetText(text)
			
			clip.SetData(dataObject) # TODO: test wstawienia
			
			clip.Flush()
			clip.Close()
			
		else:
			wx.MessageBox('Problem z dostępem do schowka', 'Kopiowanie Tekstu')
		
	#------------------------------------------------------------------
	# wstawianie tekstu ze schowka
	#------------------------------------------------------------------

	def OnPasteText(self, event):
		
		clip = wx.Clipboard().Get()
		
		if clip.Open():
		
			dataObject = wx.TextDataObject()
			
			clip.GetData(dataObject) # TODO: test pobrania
			
			text = dataObject.GetText()
			
			self.SetItemText(self.GetSelection(), text)
			
			clip.Flush()
			clip.Close()
			
		else:
			wx.MessageBox('Problem z dostępem do schowka', 'Kopiowanie Tekstu')

	#------------------------------------------------------------------
	# edycja wybranego elementu
	#------------------------------------------------------------------

	def OnEdit(self, event):
		
		selected = self.GetSelection()
		
		dlg = wx.TextEntryDialog(self, 'Tekst:', 'Edycja', self.GetItemText(selected) )
		
		if dlg.ShowModal() == wx.ID_OK :
			self.SetItemText(selected, dlg.GetValue())

	#------------------------------------------------------------------
	# kasowanie wybranego elementu i jego podelementow
	#------------------------------------------------------------------
		
	def OnDelete(self, event):
		
		dlg = wx.MessageDialog(self, 'Skasować ?', 'Usuwanie')
		
		if dlg.ShowModal() == wx.ID_OK:
			self.DeleteElement(self.GetSelection())
		
		
	def OnShowInfo(self, event):
		selected = self.GetSelection()
		pyData = self.GetPyData(selected)
		print '== info =='
		print pyData
		if pyData[2]: 
			print pyData[2].toString()
		
	#------------------------------------------------------------------
	# 
	#------------------------------------------------------------------
	
	def OnSaveSelected(self, event):
		selected = self.GetSelection()
		self.SaveSelected('selected', selected)
		print '== Save =='
	
	#------------------------------------------------------------------
	# wczytywanie danych do drzewa
	#------------------------------------------------------------------
	
	def LoadData(self):
		self.ReadRoot()
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def ReadDB(self):
		#self.db.GetStats()
		#self.db.GetViewstates()
		pass
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
		
	def ReadRoot(self):
		
		rows = self.db.GetTreeRoot()
		
		if not rows:
			#print 'Brak elemtnu'
			pass
		else:
			kontener = container.Container(rows[0])

			self.root = self.AddRoot(kontener.values['name'])
			self.SetPyData(self.root,('root', '-', None))
			self.SetItemImage(self.root, self.imageId['root'], wx.TreeItemIcon_Normal)
			
			#print 'parent:', kontener.values['parent']
			#print 'children:'
			
			for child in kontener.values['children']:
				#print child['type'], child['id']
				if child['type'] == 'folder':
					self.ReadFolder( child['id'], self.root)
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def AddFolder(self, number, parent, name = 'Nowy Folder', dane = None):
		element = self.AppendItem(parent, name)
		self.SetPyData(element,('folder', number, dane))
		self.SetItemImage(element, self.imageId['folder'], wx.TreeItemIcon_Normal)
			
	#------------------------------------------------------------------
		
	def AddLista(self, number, parent, name = 'Nowa Lista', dane = None):
		element = self.AppendItem(parent, name)
		self.SetPyData(element,('lista', number, dane))
		self.SetItemImage(element, self.imageId['lista'], wx.TreeItemIcon_Normal)
			
	#------------------------------------------------------------------
		
	def AddTask(self, number, parent, name = 'New Task', data = None):
		
		#data.Print()
		
		element = self.AppendItem(parent, name)
		self.SetPyData(element,('task', number, data))
		self.SetItemImage(element, self.imageId['task'], wx.TreeItemIcon_Normal)
		
		return element
			
	def DeleteElement(self, item):
		if self.ItemHasChildren(item):
			wx.MessageBox('element ma podelementy !!!')
		else:
			self.Delete(item)
	#------------------------------------------------------------------
		
	def ReadFolder(self, numer, parent):
		
		rows = self.db.GetTreeFolder(numer)
		
		if not rows: 
			#print 'Brak folderu'
			pass
		else:
			kontener = container.Container(rows[0])

			folder = self.AppendItem(parent, kontener.values['name'])
			self.SetPyData(folder,('folder', numer, kontener))
			
			self.SetItemImage(folder, self.imageId['folder'], wx.TreeItemIcon_Normal)
			
			for child in kontener.values['children']:
				
				if child['type'] == 'folder':
					self.ReadFolder( child['id'], folder)
					
				if child['type'] == 'list':
					self.ReadList( child['id'], folder)

				if child['type'] == 'task':
					self.ReadTask( child['id'], folder)

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def ReadList(self, numer, parent):
		
		rows = self.db.GetTreeList(numer)
		
		if not rows: 
			#print 'Brak listy'
			pass
		else:
			kontener = container.Container(rows[0])

			lista = self.AppendItem(parent, kontener.values['name'])
			self.SetPyData(lista,('lista', numer, kontener))
			
			self.SetItemImage(lista, self.imageId['lista'], wx.TreeItemIcon_Normal)
			
			for child in kontener.values['children']:
				
				if child['type'] == 'folder':
					self.ReadFolder( child['id'], lista)
					
				if child['type'] == 'list':
					self.ReadList( child['id'], lista)

				if child['type'] == 'task':
					self.ReadTask( child['id'], lista)
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def ReadTask(self, numer, parent):
		
		rows = self.db.GetTreeTask(numer)
		
		if not rows: 
			#print 'Brak zadania'
			pass
		else:
			zadanie = task.Task(rows[0])
			
			task_id = self.AddTask(numer, parent, zadanie.values['name'], zadanie)
			
			for child in zadanie.values['child_keys']:
				
				self.ReadTask( child, task_id)					

	#------------------------------------------------------------------
	# wypisywanie elementow drzewa
	#------------------------------------------------------------------

	def PrintTreeItem(self, root, poziom):
		
		data = self.GetPyData(root)
		
		if data[2]:
			dane = data[2].GetData() 
			print ','.join(dane)
			#self.writer.writerow((dane[0], dane[1]))
			self.writer.writerow(dane)
			self.db_write.SetTreeTask(dane[0],dane[1])
		else:
			print 'None'
			
		#self.writer.writerow((data[0], data[1], data[2].GetData() if data[2] else '{}') )
		
		#print 2*poziom*' ','text:', self.GetItemText(root)
		#print 2*poziom*' ','dane:', self.GetPyData(root)
		
		child, cookie = self.GetFirstChild(root)
		
		while child.IsOk():
			self.PrintTreeItem(child, poziom+1)
			
			child, cookie = self.GetNextChild(root, cookie)
	
	#------------------------------------------------------------------
	# wypisywanie elementow drzewa
	#------------------------------------------------------------------

	def PrintTreeItemText(self, selected, poziom):
		
		# tekst do zapisu
		text = self.GetItemText(selected)

		# wypisanie na ekran
		print text
		
		# tworzenie tuple z pustymi miejscami aby otrzymac wciecia
		for i in range(poziom):
			self.ofile.write(',')
			
		# zapisanie do pliku
		self.ofile.write('"%s"\n' % text)
		
		# sprawdzanie czy jest potomek
		child, cookie = self.GetFirstChild(selected)
		
		while child.IsOk():
			# przetwarzanie potomków
			self.PrintTreeItemText(child, poziom+1)
			# kolejny potomek 
			child, cookie = self.GetNextChild(selected, cookie)
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def Save(self, filename):
		
		self.ofile = codecs.open( filename + '.csv','w+')
		self.writer = csv.writer(self.ofile)
		
		self.PrintTreeItem(self.root, 0)
		
		self.ofile.close()
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

	def SaveSelected(self, filename, selected):
		
		self.ofile = codecs.open( filename + '.csv','w+', encoding="utf-8")
		self.writer = csv.writer(self.ofile)
		
		self.PrintTreeItemText(selected, 0)
		
		self.ofile.close()
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def Close(self):
		self.db.Close()
		self.db_write.Close()	
	
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
