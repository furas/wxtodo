# -*- coding: utf-8 -*-

import json

class Task(object):

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def __init__(self, data = None):
		if data:
			self.key = data[0]
			#print 'Task Values:', data[1]
			self.values = json.loads( data[1] )
		
	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def SetDatas(self, key, name, parent):
		self.key = key # pelny numer z nazwa
		self.values = {}
		self.values['name'] = name
		self.values['archive_status'] = False
		self.values['checked_status'] = False
		self.values['parent_key'] = parent
		self.values['child_keys'] = []

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def Print(self):
		print self.toString()

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def toString(self):
		
		txt = \
"""key: %s
name: %s
archive_status: %s
checked_status: %s
parent_key: %s
child_keys: %s
""" % (self.key, self.values['name'], str(self.values['archive_status']), str(self.values['checked_status']), self.values['parent_key'], '-' if not self.values['child_keys'] else "\n" + "\n".join(self.values['child_keys']))
		
		return txt

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def Dump(self):
		return json.dumps(self.values);

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------
	
	def GetData(self):
		return (self.key, self.Dump());

	#------------------------------------------------------------------
	#
	#------------------------------------------------------------------

