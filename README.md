
Creation date: 2012.08.23  
Last modification: 2012 ??   

---

Language: Python  
 Modules: wxPython  

Database: sqlite  
  Schema: from program "Hub List"  

   Icons: Tango (http://tango.freedesktop.org/Tango_Icon_Gallery)  
   
---

Hub List:  
- created in JavaScript using Ext JS 4  
- probably doesn't exist any more (http://hublistapp.com)  
- some information  
    http://pythonhackers.com/p/rjsteinert/Hub-List_GTD-Productivity  
    https://www.openhub.net/p/hub-list  

---

I created it after problems with Hub List. I use it from time to time.  
I still have some information in this database.

---

![wxToDo-1](/furas/wxtodo/raw/master/docs/wxToDo-1.jpg)
![wxToDo-2](/furas/wxtodo/raw/master/docs/wxToDo-2.jpg)
